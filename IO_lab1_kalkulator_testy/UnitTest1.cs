using Microsoft.VisualStudio.TestTools.UnitTesting;
using IO_lab1_kalkulator;
using System;

namespace IO_lab1_kalkulator_testy
{
    [TestClass]
    public class UnitTest1
    {
 
        [TestMethod]
        public void TestConst()
        {
            // Given
            string constant_string = "4";
            int expected_result = 4;


            // When
            int result = Kalkulator.Licz(constant_string);


            // Then
            Assert.AreEqual(expected_result, result);

        }
        [TestMethod]
        public void TestEmptyString()
        {
            // Given
            string empty_string = "";
            int expected_result = 0;


            // When
            int result = Kalkulator.Licz(empty_string);


            // Then
            Assert.AreEqual(expected_result, result);

        }
        [TestMethod]
        public void TestAdditionWithComma()
        {
            // Given
            string two_strings = "4,2";
            int expected_result = 6;


            // When
            int result = Kalkulator.Licz(two_strings);


            // Then
            Assert.AreEqual(expected_result, result);

        }
        [TestMethod]
        public void TestAdditionWithNewline()
        {
            // Given
            string two_strings = "4\n2";
            int expected_result = 6;


            // When
            int result = Kalkulator.Licz(two_strings);


            // Then
            Assert.AreEqual(expected_result, result);

        }
        [TestMethod]
        public void TestAdditionSpanningMultipleLines()
        {
            // Given
            string three_strings = "14\n4,3";
            int expected_result = 21;


            // When
            int result = Kalkulator.Licz(three_strings);


            // Then
            Assert.AreEqual(expected_result, result);

        }
        [TestMethod]
        public void TestAdditionWithNegativeInput()
        {
            // Given
            string three_strings = "-2,4,6";


            // When
            //int result = Kalkulator.Licz(three_strings);


            // Then
            Assert.ThrowsException<Exception>(() => Kalkulator.Licz(three_strings));

        }
        [TestMethod]
        public void TestAdditionWithBigInput()
        {
            // Given
            string three_strings = "4,1235,8";
            int expected_result = 12;


            // When
            int result = Kalkulator.Licz(three_strings);


            // Then
            Assert.AreEqual(expected_result, result);
        }
        [TestMethod]
        public void TestAdditionWithMultipleBigInputs()
        {
            // Given
            string three_strings = "1342,1235,5000";
            int expected_result = 0;


            // When
            int result = Kalkulator.Licz(three_strings);


            // Then
            Assert.AreEqual(expected_result, result);
        }
        [TestMethod]
        public void TestCustomSeparator()
        {
            // Given
            string new_separator = "//#\n4#5#6";
            int expected_result = 15;


            // When
            int result = Kalkulator.Licz(new_separator);


            // Then
            Assert.AreEqual(expected_result, result);
        }
        [TestMethod]
        public void TestCustomMultistringSeparator()
        {
            // Given
            string new_separator = "//[##]\n5##2##15";
            int expected_result = 22;


            // When
            int result = Kalkulator.Licz(new_separator);


            // Then
            Assert.AreEqual(expected_result, result);
        }
        [TestMethod]
        public void TestMultipleSeparators()
        {
            // Given
            string new_separator = "//[##][,][???]\n14##3???7";
            int expected_result = 24;


            // When
            int result = Kalkulator.Licz(new_separator);


            // Then
            Assert.AreEqual(expected_result, result);
        }
    }
}
