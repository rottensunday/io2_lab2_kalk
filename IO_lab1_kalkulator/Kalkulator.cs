﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;

namespace IO_lab1_kalkulator
{
    public static class Kalkulator
    {
        public static int Licz(string input)
        {
            int result = int.MaxValue;
            int tempResult;
            if (string.IsNullOrEmpty(input))
            {
                result = 0;
                return result;
            }
            if (IsConst(input, out result)) return result;
            List<string> separators = new List<string>();
            string pattern = @"\[(.*?)\]";
            MatchCollection matches = Regex.Matches(input, pattern);
            foreach(Match match in matches)
            {
                separators.Add(match.Groups[1].Value);
            }
            if (input[0] == '/' && input[1] == '/' && input[2] != '[') { separators.Add(input[2].ToString()); }
            separators.Add(",");
            separators.Add("\n");
            if(separators.Count > 2)
            {
                input = input.Split('\n', 2)[1];
            }
            string[] split_strings = input.Split(separators.ToArray(), StringSplitOptions.None);
            List<string> split_strings_list = new List<string>(split_strings);
            if (split_strings_list.Count < 2) return result;
            int sum = 0;
            foreach (string s in split_strings_list)
            {
                tempResult = CheckResult(s);
                sum += tempResult;
            }
            return sum;
        }

        private static int CheckResult(string input)
        {
            int tempResult;
            if (!int.TryParse(input, out tempResult)) return int.MaxValue;
            if (tempResult < 0) throw new Exception();
            if (tempResult > 1000) return 0;
            return tempResult;
        }

        private static bool IsConst(string input, out int result)
        {
            int tempResult;
            result = int.MaxValue;
            if (int.TryParse(input, out tempResult))
            {
                result = tempResult;
                if (result < 0) throw new Exception();
                if (result > 1000) result = 0;
                return true;
            }
            return false;
        }
    }
}
